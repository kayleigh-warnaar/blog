---
title: Prototyping
date: 2017-09-19
---

Vandaag volgde ik een hoorcollege over prototyping. 
Ik vond het persoonlijk een erg leuk hoorcollege doordat de leeraar erg leuk en enthousiast vertelde over het onderwerp.
Ik heb geleerd hoe je het meeste uit je prototype kan halen, welke soorten van prototyping er zijn en dat het erg belangrijk en goed is om te onderzoeken.

Na dit hoorcollege ben ik er achter gekomen dat de manier waarop ik nu een eerste prototype heb gemaakt met mijn team, misschien niet de juiste manier was en dat we bij het maken van ons volgende prototype nog beter moeten kijken naar de uitvoering hiervan.