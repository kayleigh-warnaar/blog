---
title: Kill your darlings
date: 2017-09-18
---

Vandaag moesten samen met mensen uit andere teams naar onze onderzoeken en prototypes gaan kijken en deze aan hen presenteren zodat zij ons feedback konden geven.
Nadat Imme mij haar onderzoek had laten zien en haar concept had uitgelegd via het prototype, was het mijn beurt om haar iets te vertellen over mijn onderzoek en concept.
Allereerst liet ik haar mijn doelgroep onderzoek zien, ze vond deze erg duidelijk en vond het erg slim dat ik naast een moodboard, ook een pdf met hierin twee eikpersonen had gemaakt.
Hierna liet ik mijn visuele onderzoek zie, als thema had ik 'toeristentrekpleisters' gekozen, dit onderzoek kwam ook erg goed over naar haar en ze begreep het meteen. Na het laten zien van mijn onderzoek gingen we over op het spel.
Ik liet haar als eerst kijken naar de spelregels, toen kwam ik er al snel achter dat deze voor haar nog een beetje onduidelijk waren. Ze gaf me dan ook als tip om deze nog wat duidelijker neer te zetten. H
elaas heb ik de rest van het spel niet aan haar kunnen laten zien voor feedback, want de tijd was om.

Hierna gingen we nog een pitch houden voor heel de klas. Het was de bedoeling dat een persoon uit elk team zou presenteren. Bij ons team zou Tim dit doen.
We hadden nog geen presentatie dus deze moest nog in elkaar gezet worden. Helaas hadden we daar niet zoveel tijd voor. Ik heb de presentatie uiteindelijk in elkaar gezet, maar vond hem niet zo sterk doordat ik er niet zoveel tijd aan kon geven als ik had gewilt.
Nadat Tim ons spel gepresenteerd had aan de klas, kregen we feedback.

De klas vond dat we duidelijker moesten maken wat de rol van de mol binnen ons spel was, wat is nou precies het doel van de mol?
Ook vonden ze dat we de mol niet 'de mol' moesten noemen. Dit hadden we zelf ook al bedacht, alleen waren we er nog niet aan toe gekomen om een andere naam te geven aan deze rol.
Ze vonden ook dat we duidelijker moesten maken wat de mol wint en hoe je nou eigenlijk weet wie je speelt

Wel vonden ze de challenges die we bedacht hadden voor tijdens het spel erg leuk!

Na deze presentaties kregen we onze nieuwe briefing/iteratie. Hier kregen we te horen dat we eigenlijk zo goed als opnieuw moeten beginnen met ons spel.
Hiervoor hebben we een scrum (planning via een whiteboard met hierop verschillende kopjes en post its) gemaakt, een nieuwe techniek die ik nog niet eerder heb gebruikt.

Na het maken van deze planning en de taakverdeling kregen we nog een workshop over de blog en was het weer tijd om naar huis te gaan.