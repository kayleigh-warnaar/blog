---
title: Eerste dag op school
date: 2017-09-04
---
Vandaag heb ik samen met mijn team een logo gemaakt. We heten Creative Collectors en hebben een strak logo ontworpen.

Hierna zijn we begonnen met het kijken wie wat kan binnen ons team, 
wie de teamcaptain werd (Tim) en hebben we onze teamafspraken besproken.

Na het maken van al deze afspraken zijn we begonnen met het maken 
van mindmaps. We hebben een mindamp gemaakt voor:

- Games
- Doelgroep
- Wat is een game
- Rotterdam

Ook hebben we de visie van de Hogeschool Rotterdam opgezocht.

Ik merkte dat ik na zo'n lange vakantie wel weer een beetje toe was aan school en nieuwe interessante opdrachten. Toch merkte ik dat ik in het begin wat moeite had met het onderzoeken. Tot nu toe vind ik de opdracht wel erg interessant en voel ik me goed in mijn groepje!
