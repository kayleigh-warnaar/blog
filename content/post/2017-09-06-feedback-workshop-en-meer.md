---
title: Feedback, workshop en meer...
date: 2017-09-06
---

Feedback:
Vandaag heb ik samen met mijn team om de eerste feedback gevraagd. We hebben feedback gekregen over ons 'algemene' onderzoek, ons visuele onderzoek en ons doelgroegonderzoek. (die we deels natuurlijk individueel gemaakt hebben)

Het thema wat ik had gekozen voor 'Rotterdam' was 'Toeristen trekpleisters'. Ik had meerdere trekpleisters in Rotterdam gezocht, zoals de Martkthal, Witte de With, verschillende parken en meer. Het onderzoek wat ik hiervan gedaan had was goed, ik had de bekende, maar ook wat minder bekende plekken in Rotterdam gekozen. De plekken die ik had gekozen zijn niet alleen interessant voor toeristen, maar ook voor studenten, en zeker voor studenten die nog niet zo bekend zijn in Rotterdam.

Mijn doelgroep onderzoek/moodboard, was nog niet helemaal goed. Doordat ik behoorlijk veel stockfoto's had gebruikt was de sfeer die ik mijn doelgroep wilde meegeven nog niet helemaal duidelijk. Deze moet ik dus nog aanpassen.

Na de feedback die we hebben gekregen, heb ik samen met mijn team de planning vastgelegd en de taakverdeling duidelijk neergezet in een document.

Studiecoach:
Ook heb ik vandaag voor het eerst een les gehad met de studiecoach, tijdens deze les hebben we geleerd over de verschillende rollen binnen een vergadering. Na deze les was het de bedoeling dat we zelf zouden kiezen welke vergaderrol ik zou willen oefenen en waarom. Mijn antwoord hierop was:

'Ik zou de vergaderrol van notulist willen oefenen en de vaardigheid die ik het eerste zou willen hanteren is: Niet te chaotisch te werk gaan, want ik merk dat ik hierdoor vaak dingen over het hoofd zie en vergeet. Soms vergeet ik hierdoor ook wat nou echt het belangrijkst is en kom ik niet goed uit mn woorden omdat er te veel in mijn hoofd gebeurd.'

Workshop:
Tijdens de workshop die we vandaag gehad hebben, heb ik al iets geleerd over het maken van onze blog, hier zal ik later deze maand wat meer over weten.