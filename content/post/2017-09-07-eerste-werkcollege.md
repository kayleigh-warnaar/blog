---
title: Eerste werkcollege
date: 2017-09-07
---

Vandaag heb ik mijn eerste werkcollege gehad. Tijdens de opdracht was het de bedoeling dat we een proces die we eerder hadden uitgevoerd zouden visualiseren. Ik had gekozen voor een zomerkamp die ik elk jaar organiseer. Nadat ik deze helemaal visueel had uitgewerkt met mijn onderzoek, concept en eindresultaat daarin, was het de bedoeling dat we de visualisaties met ons team zouden bespreken en de duidelijkste zouden uitkiezen en verder zouden uitwerken.

Uiteindelijk kozen we de visualisatie van Lianne, zij ging stage lopen in Engeland en had hier een mooi proces bij gevisualiseerd. Deze hebben we nog aangepast met wat extra info, en toen gepresenteerd. Later bleek wel dat we niet echt een proces voor een bepaalde doelgroep hadden laten zien, maar meer voor iemand zelf.

Na deze les heb ik samen met mijn team nog wat gebrainstormd over de design challenge opdracht. We hadden verschillende leuke ideeën, maar kwamen er ook achter dat sommige dingen weer lastig te combineren waren met onze doelgroep. Daarom hebben we ervoor gekozen allemaal onze eigen concepten uit te schrijven en tekenen en deze maandag te bespreken.