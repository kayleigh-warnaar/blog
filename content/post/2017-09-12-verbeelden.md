---
title: Verbeelden
date: 2017-09-12
---
Vandaag heb ik mijn 2e hoorcollege gehad 'verbeelden'.
Ik heb wat geleerd over:

 - Verbeelden;
 - Communicatie - de zender, ontvanger en boodschap -
 - Visuele communicatie;
 - Waarnemen;
 - De Wet van Gestalt;
 - Semiotiek
 - Retorica en meer...

Hierna heb ik thuis gewerkt aan het maken van mijn blog. Mijn eigen blogpagina staat online, alleen kreeg ik er helaas geen post op. Ik heb verschillende dingen geprobeerd maar heb het helaas niet voor elkaar gekregen, dit moet ik dus nog een keer navragen.
