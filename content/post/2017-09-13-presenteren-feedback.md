---
title: Presenteren en feedback
date: 2017-09-13
---
Vandaag zijn we verder gegaan aan ons spel. We hebben het prototype van de app nog wat aangepast, en de spelregels opnieuw opgeschreven. Hierna moesten we ons prototype presenteren aan de klas.

De feedback die we kregen was:

 - Het spel wat te versimpelen
 - Een wat betere rol voor onze mol verzinnen
 Ook kregen we te horen dat de andere leerlingen dachten elkaar wel goed te leren kennen via het spel.

Hierna had ik een workshop over het kijken en lezen van beelden. Dit was erg interessant, ik heb hier op het MBO al geleerd maar kwam erachter dat ik al weer veel van deze dingen vergeten was. Een goede opfrisser dus!

Na school ben ik verder gegaan met mijn blog, deze stond al wel online maar mijn posts wilde niet op mijn blog komen te staan... helaas lukte dit na vandaag nog steeds niet.