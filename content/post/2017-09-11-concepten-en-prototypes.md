---
title: Concepten en prototypes
date: 2017-09-11
---

Vandaag heb ik samen met mijn team gekeken naar al de verschillende concepten die iedereen tijdens het weekend had uitgeschreven/getekend. We kwamen er achter dat het interessant was om meerdere ideeën aan elkaar te verbinden.

Na wat gebrainstormd etc. zijn we op een heel nieuw concept gekomen en besloten we hiermee verder te gaan. We hebben samen verschillende regels bedacht en bedacht wat er wel en niet in het spel moest komen.

Nadat we dit hadden besproken begon ik samen met Lianne aan het maken van het prototype voor het online gedeelte van het spel, een app. Na een tijdje kwam er een leraar bij ons staan en gaf ons wat feedback over het prototype.

 - Zo moesten we denken aan de magic circle, was alles duidelijk?
 - Moesten we kijken naar 'meaningfull play' zoals pop ups die aan kunnen geven hoe de teams er bijv. voor staan binnen het spel.
 - Ook gaf hij aan of we misschien wat knoppen moesten gebruiken met 'terug'.

Deze feedback hebben we gelijk toegepast aan ons prototype.
