---
title: Spel analyseren!
date: 2017-09-14
---

Vandaag heb ik mijn eerste les met Sketch gehad, persoonlijk vind ik het programma erg op illustrator lijken, tot nu toe kreeg ik alles aardig snel onder de knie. Ik ben benieuwd hoe dit over een paar weken zal gaan!

Na mijn keuzevak, Sketch, ben ik met mijn groepje samengekomen om ons eerste concept te analyseren.
We besloten een rustige ruimte op te zoeken en begonnen daar met de analyse. Tim was de observant, ik was de fotograaf en Lianne, Ylva en Florris speelden het spel.
Tijdens het lezen van de spelregels kwamen we erachter dat deze nog niet duidelijk genoeg waren en dat ze van het een zo naar het ander sprongen, hierdoor was het erg onduidelijk en ontstond er een kleine discussie tussen de spelers van het spel.
Ook kwamen we erachter dat het spel/de spelregels nog niet erg motiverend genoeg was om te spelen. Wel merkte we dat de spelers plezier hadden in het spelen van het spel. 

Na deze analyse gingen we terug naar school en hebben we onze verschillende ontdekkingen in een pdf gezet.

Nadat we dit gedaan hadden probeerde ik nog even verder te werken aan mijn blog tot de volgende les zou beginnen. Helaas kwam hier nog steeds niets uit, ook niet nadat andere klasgenoten hiernaar hadden gekeken...

Tijdens ons werkcollege gingen we wat verder in over de wet van gestalt en hoe deze op verschillende manieren wordt toegepast. Erg interresant en leerzaam!

Na school koos ik er voor om toch nog even verder te gaan met mijn blog. Na het maken van een hele nieuwe pagina en veel gefrustreerde minuten later, deed mijn eerste post het eindelijk!
Vanaf vandaag kan ik dus al mijn posts echt posten op mijn blog!