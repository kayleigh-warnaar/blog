---
title: Spellen analyseren
date: 2017-09-20
---

Vandaag heb ik samen met mijn team vershillende bestaande spellen geanalyseerd.
We hebben gekozen voor de spellen: 30 Seconds, Skipbo en Weerwolven van Wakkerdam.

Na het analyseren van deze spellen volgde ik een workshop 'Creative technieken'.
Tijdens de workshop heb ik meer geleerd over de divergerende en convergerende fase.
Ik heb verschillende manieren van onderzoeken geleerd, technieken waarvan ik voor de workshop nog nooit gehoord had.
Er zaten technieken bij waarvan ik ze graag eens zou willen uitproberen, een hiervan was 'Negatief brainstormen'.

Na de workshop heb ik verder gewerkt aan het onderzoek voor ons spel.
Ik heb mijn visuele onderzoek geïnspireerd op het eten en de plekken waar je kan eten/eten kan halen in Rotterdam. 
Tijdens het onderzoeken van deze vraag heb ik erg goed gekeken naar studenten vaak hun eten vandaan halen 
of waar ze graag gaan eten. Ook heb ik gekeken wat ze dan graag eten. 

Een van de dingen die studenten allemaal erg lekker vinden is een kapsalon, deze is ook erg Rotterdams, 
deze kon dus niet missen in mijn moodboard. 

Bij eten hoort natuurlijk ook een stukje cultuur, en waar kan je dan beter terecht dan bij de Markthal, de Markt of 
de Witte de With?! Er zijn naast deze plekken natuurlijk nog super veel andere plekken waar je terecht kan voor 
verschillende type gerechten en snacks. Ook in Rotterdam komen er steeds meer plekken om lekker te eten of om eten te kopen, er komen ook steeds meer nieuwe cafeetjes en barretjes bij. Ook pop up winkels en restaurants zijn erg populair. Zo was de pindakaaswinkel een tijdje terug voor een tijdje te vinden in de Markthal. 

Door al deze verschillende leuke plekken met leuke eetgelegenheden zullen studenten in Rotterdam veel van 
verschillende culturen proeven en hoeven ze geen dag hetzelfde te eten!